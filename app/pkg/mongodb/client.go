package mongodb

import (
	"context"
	"log"
	"os"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

// GetClient осуществляет подключение к БД.
func ConnectDB() (*mongo.Client, context.Context, context.CancelFunc, error) {
	credential := options.Credential{
		Username: os.Getenv("MONGO_ROOT_USERNAME"),
		Password: os.Getenv("MONGO_ROOT_PASSWORD"),
	}

	var duration time.Duration = 10

	clientOptions := options.Client().ApplyURI("mongodb://suffragia_mongo").SetAuth(credential)

	ctx, cancel := context.WithTimeout(context.Background(), duration*time.Second)

	client, errs := mongo.Connect(ctx, clientOptions)
	if errs != nil {
		log.Fatalf("ошибка подключения к mongoDB: %s", errs.Error())
		return client, ctx, cancel, errs
	}

	if errs = client.Ping(ctx, readpref.Primary()); errs != nil {
		log.Fatalf("ошибка проверки подключения к mongoDB: %s", errs.Error())
		return client, ctx, cancel, errs
	}

	return client, ctx, cancel, errs
}

// DisconnectDB осуществляет отключени от БД.
func DisconnectDB(ctx context.Context, client *mongo.Client, cancel context.CancelFunc) {
	defer cancel()
	defer func() {
		if err := client.Disconnect(ctx); err != nil {
			panic(err)
		}
	}()
}
