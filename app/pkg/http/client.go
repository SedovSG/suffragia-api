package http

import (
	"bytes"
	"net/http"
	"time"
)

// Метод осуществляет запрос.
func SendRequest(query, method string, data []byte, headers map[string]interface{}) (result *http.Response, err error) {
	client := &http.Client{Timeout: time.Second * 10}

	request, err := http.NewRequest(method, query, bytes.NewBuffer(data))
	if err != nil {
		return result, err
	}

	for key, val := range headers {
		request.Header.Set(key, val.(string))
	}

	response, errs := client.Do(request)
	if errs != nil {
		return result, err
	}

	statusOK := response.StatusCode >= http.StatusOK && response.StatusCode < http.StatusBadRequest

	if !statusOK {
		return result, err
	}

	return response, err
}
