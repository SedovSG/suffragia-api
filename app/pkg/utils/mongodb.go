package utils

import (
	"math"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"suffragia/internal/api/v1/resources"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// Параметры запроса.
type Params map[string]string

type PaginatorResponse map[string]interface{}

// PaginationForResponse формирует карту с постраничной навигацией в качестве ответа.
func PaginationForResponse(dataModel resources.IResponse, vars url.Values, total int64) PaginatorResponse {
	var code int16 = 200
	status := "OK"

	result := map[string]interface{}{
		"code":   code,
		"status": status,
		"data": map[string]interface{}{
			"total":  total,
			"models": dataModel,
		},
	}

	limit, _ := strconv.ParseInt(vars.Get("limit"), 10, 64)
	start, _ := strconv.ParseInt(vars.Get("start"), 10, 64)

	if limit > 0 {
		result["data"].(map[string]interface{})["pageSize"] = limit
	}

	if start > 0 {
		result["data"].(map[string]interface{})["pageNum"] = start
	}

	if limit > 0 && start > 0 {
		pageCount := math.Ceil(float64(total) / float64(limit))
		result["data"].(map[string]interface{})["pageCount"] = pageCount
	}

	if vars.Get("sort") != "" && strings.Index(vars.Get("sort"), "::") > 0 {
		result["data"].(map[string]interface{})["sort"] = vars.Get("sort")
	}

	if len(result["data"].(map[string]interface{})["models"].([]bson.M)) == 0 {
		result["code"] = http.StatusNotFound
		result["errors"] = http.StatusText(http.StatusNotFound)
		result["status"] = ResponseStatusError
		result["data"] = map[string]interface{}{}
	}

	return result
}

// FilteringFromRequest формирует фильтр из строки запроса.
//
// Пример использования аргумента:
// 	filter=field1::value1,field2::value2.
func FilteringFromRequest(vars url.Values) Params {
	result := make(Params)

	if vars.Get("filter") != "" && strings.Index(vars.Get("filter"), "::") > 0 {
		parse := strings.Split(vars.Get("filter"), ",")

		for _, filter := range parse {
			item := strings.Split(strings.Trim(filter, " "), "::")
			result[item[0]] = item[1]
		}
	}

	return result
}

// SortingFromRequest формирует сортировку из строки запроса.
//
// Пример использования аргумента:
// 	sort=field1::asc,field2::desc.
func SortingFromRequest(vars url.Values) Params {
	result := make(Params)

	if vars.Get("sort") != "" && strings.Index(vars.Get("sort"), "::") > 0 {
		parse := strings.Split(vars.Get("sort"), ",")

		for _, filter := range parse {
			item := strings.Split(strings.Trim(filter, " "), "::")

			result["field"] = item[0]
			result["order"] = item[1]
		}
	}

	return result
}

// FindOptions параметры ограничения выборки для операции поиска.
//
// Можно использовать следующие аргументы строки запроса: limit | start | sort
//
// Пример использования аргументов:
// 	start=2&limit=5&sort=field1::asc,field2::desc.
func FindOptions(vars url.Values, sort Params) *options.FindOptions {
	findOptions := options.Find()

	limit, _ := strconv.ParseInt(vars.Get("limit"), 10, 16)
	start, _ := strconv.ParseInt(vars.Get("start"), 10, 16)

	if limit != 0 {
		findOptions.SetLimit(limit)
	}

	if start != 0 {
		start = limit * (start - 1)
		findOptions.SetSkip(start)
	}

	if sort["order"] == "asc" {
		findOptions.SetSort(bson.D{primitive.E{Key: sort["field"], Value: 1}})
	} else if sort["order"] == "desc" {
		findOptions.SetSort(bson.D{primitive.E{Key: sort["field"], Value: -1}})
	}

	return findOptions
}

// IsHave проверяет наличие параметров фильтрации данных.
func (param Params) IsHave() bool {
	return len(param) > 0
}

// AggregateOptions параметры ограничения выборки для операции агрегации.
//
// Можно использовать следующие аргументы строки запроса: limit | start | sort
//
// Пример: start=2&limit=5&sort=field1::asc,field2::desc.
func AggregateOptions(pipeline mongo.Pipeline, vars url.Values, sort Params) mongo.Pipeline {
	limit, _ := strconv.ParseInt(vars.Get("limit"), 10, 16)
	start, _ := strconv.ParseInt(vars.Get("start"), 10, 16)

	if skipStage, ok := primitiveSkip(start, limit); ok {
		pipeline = append(pipeline, bson.D{skipStage})
	}

	if limitStage, ok := primitiveLimit(limit); ok {
		pipeline = append(pipeline, bson.D{limitStage})
	}

	if sortStage, ok := primitiveSort(sort); ok {
		pipeline = append(pipeline, bson.D{sortStage})
	}

	return pipeline
}

// primitiveLimit примитив предствления функции $limit MongoDB.
func primitiveLimit(limit int64) (bson.E, bool) {
	var result bson.E

	if limit != 0 {
		result = bson.E{Key: "$limit", Value: limit}
	}

	return result, limit > 0
}

// primitiveSkip примитив предствления функции $skip MongoDB.
func primitiveSkip(start, limit int64) (bson.E, bool) {
	var result bson.E

	if start != 0 {
		start = limit * (start - 1)
		result = bson.E{Key: "$skip", Value: start}
	}

	return result, start > 0
}

// primitiveSort примитив предствления функции $sort MongoDB.
func primitiveSort(sort map[string]string) (bson.E, bool) {
	var result bson.E

	if sort["order"] == "asc" {
		result = bson.E{Key: "$sort", Value: bson.D{
			bson.E{Key: sort["field"], Value: 1},
		}}
	} else if sort["order"] == "desc" {
		result = bson.E{Key: "$sort", Value: bson.D{
			bson.E{Key: sort["field"], Value: -1},
		}}
	}

	return result, len(sort) > 0
}
