package utils

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"suffragia/internal/api/v1/resources"

	"github.com/SedovSG/zaplog"
)

// Write структура построения HTTP-ответа.
type Write struct {
	IO http.ResponseWriter
}

func Uniqid(prefix string) string {
	now := time.Now()
	sec := now.Unix()
	//nolint:gomnd // Для повышения производительности
	usec := now.UnixNano() % 0x100000

	return fmt.Sprintf("%s%08x%05x", prefix, sec, usec)
}

const (
	// ResponseStatusOK в случае успешного запроса.
	ResponseStatusOK string = "success"
	// ResponseStatusError в случае ошибке в запросе.
	ResponseStatusError string = "error"
)

// Response возвращает структуру построения HTTP-ответа.
func Response(resp http.ResponseWriter) Write { return Write{IO: resp} }

// EncodeErrorToJSON кодирует сообщение в JSON формат.
func (resp Write) MessageToJSON(code int, message ...string) {
	var result resources.IResponse

	if len(message) > 0 {
		result = resources.Response{Code: code, Errors: message, Data: map[string]interface{}{}, Status: ResponseStatusError}
	} else {
		result = resources.Response{Code: code, Errors: http.StatusText(code), Data: map[string]interface{}{}, Status: ResponseStatusError}
	}

	resp.IO.WriteHeader(code)
	resp.EncodeToJSON(result)
}

// EncodeToJSON кодирует ответ в JSON формат.
func (resp Write) EncodeToJSON(result resources.IResponse) {
	encoder := json.NewEncoder(resp.IO)
	if err := encoder.Encode(result); err != nil {
		zaplog.Throw().Error("Ошибка кодирования json: " + err.Error())
	}
}
