PROJECT_NAME=$(shell basename "$(PWD)")
type?=yaml

.PHONY: init
init: ## Инициализировать модуль проекта
	@go mod init ${PROJECT_NAME}
	@go mod tidy
	@echo "Модуль ${PROJECT_NAME} успошно инициализирован"

.PHONY: setup
setup: ## Установить зависимости: mode=stage|prod
ifeq ("$(wildcard ./go.mod)","")
	$(MAKE) init
endif

ifeq ($(mode),dev)
	@go install github.com/githubnemo/CompileDaemon
	@go get github.com/stretchr/testify
	@go install github.com/rakyll/gotest
	@go install github.com/golangci/golangci-lint/cmd/golangci-lint@v1.52.2
else ifeq ($(mode),ci)
	@go get -u github.com/stretchr/testify
	@go install github.com/golangci/golangci-lint/cmd/golangci-lint@v1.52.2
endif
	@go get -u github.com/gorilla/mux
	@go get github.com/asaskevich/govalidator
	@go get go.mongodb.org/mongo-driver/mongo
	@go get github.com/SedovSG/zaplog
	@echo "Зависимости успешно установлены"


.PHONY: run
run: ## Скомпилировать и запустить основной пакет
	@CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go run main.go

.PHONY: build
build: ## Собрать версию программы
	@CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o /go/src/bin/main
	@echo "Программа успешно скомпилирована"

.PHONY: format
format: ## Отформатировать исходный код
	@gofmt -w . && echo "Исходный код отформатирован"

.PHONY: lint
lint: ## Проверить исходный кода на соответствие стандартам
	@golangci-lint run ./... && echo "Выполнено успешно"

.PHONY: test
test: ## Запустить выполнение тестов
	@go test -v ./internal/api/v1/routes
	@rm -rf storage/tmp/

.PHONY: cover
cover: ## Запустить отчёт о покрытии кода тестами
ifeq ($(type),html)
	@gotest ./internal/api/.../routes -coverpkg=./... -coverprofile=coverage.out
	@go tool cover -html=coverage.out -o coverage.html
else
	@gotest ./internal/api/.../routes -coverpkg=./... -coverprofile=coverage.out
	@go tool cover -func=coverage.out
endif

.PHONY: clean
clean: ## Удалить временные файлы
	@go clean && rm -rf bin/ && echo "Временные файлы удалены"

.PHONY: swagger
swagger: ## Сгенерировать файл конфигурации Swagger в формате JSON/YAML: type=yaml|json
	@GO111MODULE=off swagger generate spec -o ./api/openapi-spec/swagger_$(prefix).$(type)
	@echo "Файл конфигурации swagger.$(type) успешно cгенерирован"

.PHONY: help
help: ## Показать справку
	@ echo '  Использование: make [КЛЮЧ...] [ОПЦИЯ...]'
	@ echo ''
	@ echo '  Ключи:'
	@ grep -E '^[a-zA-Z_-]+:.*?## .*?$$' $(firstword $(MAKEFILE_LIST)) | awk 'BEGIN {FS = ":.*?## "}; \
	{printf "    \033[36m%-30s\033[0m %s\n", $$1, $$2, $$3}' | column -t -s ':'

.DEFAULT_GOAL := help
