package services

import (
	"errors"
	"net/http"

	"suffragia/internal/api/v1/models"
	"suffragia/internal/api/v1/resources"
	"suffragia/pkg/utils"

	"github.com/gorilla/mux"
)

type Election models.Election

func (p *Election) Get(resp http.ResponseWriter, req *http.Request) (res resources.ElectionResponse, err error) {
	vars := mux.Vars(req)

	com, err := Committees(vars["subject_id"], vars["uik"])
	if errors.Is(err, ContentMissingError) {
		utils.Response(resp).MessageToJSON(http.StatusOK, ContentMissingError.Error())
		return
	}

	if err != nil {
		utils.Response(resp).MessageToJSON(http.StatusInternalServerError, err.Error())
		return res, errors.New("Ошибка в запросе: " + err.Error())
	}

	elec, err := Elections(com.Vrn)
	if errors.Is(err, ContentMissingError) {
		utils.Response(resp).MessageToJSON(http.StatusOK, ContentMissingError.Error())
		return
	}

	if err != nil {
		utils.Response(resp).MessageToJSON(http.StatusInternalServerError, err.Error())
		return res, errors.New("Ошибка в запросе: " + err.Error())
	}

	elec[0].VrnKomis = com.Vrn

	res.Code = http.StatusOK
	res.Status = utils.ResponseStatusOK
	res.Data = elec

	return res, err
}
