package services

import (
	"errors"
	"net/http"

	"suffragia/internal/api/v1/models"
	"suffragia/internal/api/v1/resources"
	"suffragia/pkg/utils"

	"github.com/gorilla/mux"
)

type Comission models.Comission

func (p *Comission) Get(resp http.ResponseWriter, req *http.Request) (res resources.ComissionResponse, err error) {
	vars := mux.Vars(req)

	comis, err := Comissions(vars["vrn_election"], vars["vrn_komis"])
	if errors.Is(err, ContentMissingError) {
		utils.Response(resp).MessageToJSON(http.StatusOK, ContentMissingError.Error())
		return
	}

	if err != nil {
		utils.Response(resp).MessageToJSON(http.StatusInternalServerError, err.Error())
		return res, errors.New("Ошибка в запросе: " + err.Error())
	}

	res.Code = http.StatusOK
	res.Status = utils.ResponseStatusOK
	res.Data = comis

	return res, err
}
