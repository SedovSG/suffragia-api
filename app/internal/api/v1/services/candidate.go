package services

import (
	"errors"
	"net/http"

	"suffragia/internal/api/v1/models"
	"suffragia/internal/api/v1/resources"
	"suffragia/pkg/utils"

	"github.com/gorilla/mux"
)

type Candidate models.Candidate

func (p *Candidate) Get(resp http.ResponseWriter, req *http.Request) (res resources.CandidateResponse, err error) {
	vars := mux.Vars(req)

	cand, err := Candidates(vars["vrn_election"], vars["num_tvd"])
	if errors.Is(err, ContentMissingError) {
		utils.Response(resp).MessageToJSON(http.StatusOK, ContentMissingError.Error())
		return
	}

	if err != nil {
		utils.Response(resp).MessageToJSON(http.StatusInternalServerError, err.Error())
		return res, errors.New("Ошибка в запросе: " + err.Error())
	}

	res.Code = http.StatusOK
	res.Status = utils.ResponseStatusOK
	res.Data = cand.Embedded.CandidateDtoList

	return res, err
}
