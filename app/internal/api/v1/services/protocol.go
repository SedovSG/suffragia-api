package services

import (
	"encoding/json"
	"errors"
	"net/http"

	"suffragia/internal/api/v1/models"
	"suffragia/internal/api/v1/resources"
	"suffragia/internal/api/v1/store"
	"suffragia/pkg/mongodb"
	"suffragia/pkg/utils"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Protocols models.Protocol

// Add Сохранение протокола голосования.
func (p *Protocols) Save(resp http.ResponseWriter, req *http.Request) (res resources.Response, err error) {
	if err = json.NewDecoder(req.Body).Decode(&p); err != nil {
		utils.Response(resp).MessageToJSON(http.StatusBadRequest, err.Error())
		return res, errors.New("некорректный запрос: " + err.Error())
	}

	data := &store.Protocol{
		ID:    primitive.NewObjectID(),
		Phone: p.Phone,
		Data: struct {
			CandidateRating           []interface{} "bson:\"candidate_rating\""
			CandidateList             []interface{} "bson:\"candidate_list\""
			Date                      string        "bson:\"date\""
			NumBallots                string        "bson:\"num_ballots\""
			NumBallotsInBoxes         string        "bson:\"num_ballots_in_boxes\""
			NumBallotsInPortableBoxes string        "bson:\"num_ballots_in_portable_boxes\""
			NumBallotsIndoor          string        "bson:\"num_ballots_indoor\""
			NumBallotsOutdoor         string        "bson:\"num_ballots_outdoor\""
			NumDestoyBallots          string        "bson:\"num_destoy_ballots\""
			NumEarlyBallots           string        "bson:\"num_early_ballots\""
			NumInvalidBallots         string        "bson:\"num_invalid_ballots\""
			NumLostBallots            string        "bson:\"num_lost_ballots\""
			NumUnaccountedBallots     string        "bson:\"num_unaccounted_ballots\""
			NumValidBallots           string        "bson:\"num_valid_ballots\""
			NumVoters                 string        "bson:\"num_voters\""
			UikID                     string        "bson:\"uik_id\""
			UikNumber                 string        "bson:\"uik_number\""
		}(p.Data),
	}

	opts := options.Update().SetUpsert(true)
	filter := bson.M{"data.uik_id": p.Data.UikID, "data.date": p.Data.Date}
	update := bson.M{"$set": data}

	db, ctx, cancel, err := mongodb.ConnectDB()
	if err != nil {
		return res, errors.New("ошибка соединения с БД: " + err.Error())
	}
	defer mongodb.DisconnectDB(ctx, db, cancel)

	protocols := db.Database("suffragia").Collection("protocols")

	upsert, err := protocols.UpdateOne(ctx, filter, update, opts)
	if err != nil {
		return res, err
	}

	if upsert.MatchedCount == 1 || upsert.ModifiedCount == 1 || upsert.UpsertedCount == 1 {
		return resources.Response{Code: http.StatusOK, Status: utils.ResponseStatusOK, Data: upsert}, err
	}

	return res, err
}

// Get Получение протоколов голосования.
func (p *Protocols) Get(resp http.ResponseWriter, req *http.Request) (res utils.PaginatorResponse, err error) {
	sortParams := utils.SortingFromRequest(req.URL.Query())
	filterParams := utils.FilteringFromRequest(req.URL.Query())

	db, ctx, cancel, err := mongodb.ConnectDB()
	if err != nil {
		return res, errors.New("ошибка соединения с БД: " + err.Error())
	}
	defer mongodb.DisconnectDB(ctx, db, cancel)

	protocols := db.Database("suffragia").Collection("protocols")

	var (
		filter     bson.M
		elements   []bson.M
		value      interface{}
		projection bson.M
	)

	if filterParams.IsHave() {
		for key, val := range filterParams {
			value = val

			if key == "_id" {
				value, _ = primitive.ObjectIDFromHex(val)
			}

			filter = bson.M{key: value}

			if key == "data.candidate_rating" {
				projection = bson.M{key: 1}
				filter = nil
			}
		}
	}

	cursor, errs := protocols.Find(ctx, filter, utils.FindOptions(req.URL.Query(), sortParams).SetProjection(projection))
	if errs != nil {
		return res, errors.New("внутренняя ошибка сервера: " + errs.Error())
	}
	defer cursor.Close(ctx)

	errs = cursor.All(ctx, &elements)
	if errs != nil {
		return res, errors.New("внутренняя ошибка сервера: " + errs.Error())
	}

	total, errs := protocols.CountDocuments(ctx, filter)
	if errs != nil {
		return res, errors.New("внутренняя ошибка сервера: " + errs.Error())
	}

	dataModel := utils.PaginationForResponse(elements, req.URL.Query(), total)

	return dataModel, err
}

// Total получает кол-во голосов.
func (p *Protocols) Total(resp http.ResponseWriter, req *http.Request) (res resources.Response, err error) {
	db, ctx, cancel, err := mongodb.ConnectDB()
	if err != nil {
		return res, errors.New("ошибка соединения с БД: " + err.Error())
	}
	defer mongodb.DisconnectDB(ctx, db, cancel)

	protocols := db.Database("suffragia").Collection("protocols")

	var (
		elements []bson.M
	)

	unwindStage := bson.D{
		bson.E{
			Key: "$unwind", Value: bson.D{
				bson.E{Key: "path", Value: "$data.candidate_list"},
				bson.E{Key: "preserveNullAndEmptyArrays", Value: true},
			},
		},
	}

	groupStage := bson.D{
		bson.E{
			Key: "$group", Value: bson.D{
				bson.E{Key: "_id", Value: "$data.candidate_list.item.index"},
				bson.E{Key: "candidate", Value: bson.D{
					bson.E{Key: "$first", Value: "$data.candidate_list"},
				}},
				bson.E{Key: "total_votes", Value: bson.D{
					bson.E{Key: "$sum", Value: bson.D{
						bson.E{Key: "$toInt", Value: "$data.candidate_list.item.votes"},
					}},
				}},
			},
		},
	}

	sortStage := bson.D{
		bson.E{
			Key: "$sort", Value: bson.D{
				bson.E{Key: "total_votes", Value: -1},
			},
		},
	}

	pipeline := append(mongo.Pipeline{}, unwindStage, groupStage, sortStage)

	cursor, errs := protocols.Aggregate(ctx, pipeline)
	if errs != nil {
		return res, errors.New("внутренняя ошибка сервера: " + errs.Error())
	}
	defer cursor.Close(ctx)

	err = cursor.All(ctx, &elements)
	if errs != nil {
		return res, errors.New("внутренняя ошибка сервера: " + errs.Error())
	} else if len(elements) == 0 {
		return res, mongo.ErrNoDocuments
	}

	res.Code = http.StatusOK
	res.Status = utils.ResponseStatusOK
	res.Data = elements

	return res, err
}
