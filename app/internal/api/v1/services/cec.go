package services

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"

	"suffragia/internal/api/v1/resources/cec"
	client "suffragia/pkg/http"
)

var ContentMissingError = errors.New("Данные осутствуют")

// Committees Получает от ЦИК информацию об УИК.
func Committees(subjectID, uik string) (res cec.CommitteResponse, err error) {
	query := fmt.Sprintf("http://www.cikrf.ru/iservices/voter-services/committee/subjcode/%s/num/%s", subjectID, uik)

	headers := map[string]interface{}{
		"User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/110.0",
	}

	result, err := client.SendRequest(query, http.MethodGet, []byte{}, headers)
	if err != nil {
		return res, errors.New("Ошибка в запросе: " + err.Error())
	}
	defer result.Body.Close()

	if result.ContentLength == 0 {
		return res, ContentMissingError
	}

	body, err := io.ReadAll(result.Body)
	if err != nil {
		return res, err
	}

	var com cec.CommitteResponse

	if err = json.Unmarshal(body, &com); err != nil {
		return res, errors.New("Ошибка в запросе: " + err.Error())
	}

	return com, err
}

// Elections Получает от ЦИК информацию о доступных выборах в УИК.
func Elections(vrnCommity string) (res cec.ElectionResponse, err error) {
	query := fmt.Sprintf("http://www.cikrf.ru/iservices/voter-services/vibory/committee/%s", vrnCommity)

	headers := map[string]interface{}{
		"User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/110.0",
	}

	result, err := client.SendRequest(query, http.MethodGet, []byte{}, headers)
	if err != nil {
		return res, errors.New("Ошибка в запросе: " + err.Error())
	}
	defer result.Body.Close()

	if result.ContentLength == 0 {
		return res, ContentMissingError
	}

	body, err := io.ReadAll(result.Body)
	if err != nil {
		return res, err
	}

	var elec cec.ElectionResponse

	if err = json.Unmarshal(body, &elec); err != nil {
		return res, errors.New("Ошибка в запросе: " + err.Error())
	}

	return elec, err
}

func Candidates(vrnElection string, numTvd string) (res cec.CandidateResponse, err error) {
	query := fmt.Sprintf("http://www.cikrf.ru/iservices/sgo-visual-rest/vibory/%s/candidates?page=1&numokr=%s", vrnElection, numTvd)

	headers := map[string]interface{}{
		"User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/110.0",
	}

	result, err := client.SendRequest(query, http.MethodGet, []byte{}, headers)
	if err != nil {
		return res, errors.New("Ошибка в запросе: " + err.Error())
	}
	defer result.Body.Close()

	if result.ContentLength == 0 {
		return res, ContentMissingError
	}

	body, err := io.ReadAll(result.Body)
	if err != nil {
		return res, err
	}

	var cand cec.CandidateResponse

	if err = json.Unmarshal(body, &cand); err != nil {
		return res, errors.New("Ошибка в запросе: " + err.Error())
	}

	return cand, err
}

func Comissions(vrnElection string, vrnKomis string) (res cec.ComissionResponse, err error) {
	query := fmt.Sprintf("http://www.cikrf.ru/iservices/sgo-visual-rest/vibory/%s/tvd/?vrnkomis=%s", vrnElection, vrnKomis)

	headers := map[string]interface{}{
		"User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/110.0",
	}

	result, err := client.SendRequest(query, http.MethodGet, []byte{}, headers)
	if err != nil {
		return res, errors.New("Ошибка в запросе: " + err.Error())
	}
	defer result.Body.Close()

	if result.ContentLength == 0 {
		return res, ContentMissingError
	}

	body, err := io.ReadAll(result.Body)
	if err != nil {
		return res, err
	}

	var comis cec.ComissionResponse

	if err = json.Unmarshal(body, &comis); err != nil {
		return res, errors.New("Ошибка в запросе: " + err.Error())
	}

	return comis, err
}
