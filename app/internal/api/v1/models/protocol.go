package models

// Protocol модель протокола голосования.
//
// swagger:model Protocol
type Protocol struct {
	// Номер телефона наблюдателя
	Phone string `json:"phone"`
	// Результаты референдума
	Data struct {
		// Рейтинги кандидатов
		CandidateRating []interface{} `json:"candidate_rating"`
		// Список кандидатов и голосов
		CandidateList []interface{} `json:"candidate_list"`
		// Дата проведения референдума
		Date string `json:"date"`
		// Число бюллетеней, полученных УИК
		NumBallots string `json:"num_ballots"`
		// Число бюллетеней в стационарных ящиках
		NumBallotsInBoxes string `json:"num_ballots_in_boxes"`
		// Число бюллетеней в переносных ящиках
		NumBallotsInPortableBoxes string `json:"num_ballots_in_portable_boxes"`
		// Число бюллетеней для голосования в помещении
		NumBallotsIndoor string `json:"num_ballots_indoor"`
		// Число бюллетеней для голосования вне помещения
		NumBallotsOutdoor string `json:"num_ballots_outdoor"`
		// Число уничтоженных бюллетеней
		NumDestoyBallots string `json:"num_destoy_ballots"`
		// Число бюллетеней для голосования досрочно
		NumEarlyBallots string `json:"num_early_ballots"`
		// Число недействительных бюллетеней
		NumInvalidBallots string `json:"num_invalid_ballots"`
		// Число утраченных бюллетеней
		NumLostBallots string `json:"num_lost_ballots"`
		// Число неучтённых бюллетеней
		NumUnaccountedBallots string `json:"num_unaccounted_ballots"`
		// Число действительных бюллетеней
		NumValidBallots string `json:"num_valid_ballots"`
		// Число избирателей на момент окончания голосования
		NumVoters string `json:"num_voters"`
		// Идентификатор УИК
		UikID string `json:"uik_id"`
		// Номер УИК
		UikNumber string `json:"uik_number"`
	} `json:"data"`
}
