package middlewares

import (
	"net/http"
	"strings"

	"github.com/SedovSG/zaplog"
)

func Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(response http.ResponseWriter, request *http.Request) {
		response.Header().Set("Content-Type", "application/json; charset=UTF-8")

		if strings.Contains(request.URL.RequestURI(), "/api/v1/swagger") {
			response.Header().Set("Access-Control-Allow-Origin", "*")
			next.ServeHTTP(response, request)
		}

		if request.Method != http.MethodPost && request.Method != http.MethodGet && request.Method != http.MethodHead &&
			request.Method != http.MethodPut && request.Method != http.MethodDelete {
			response.WriteHeader(http.StatusMethodNotAllowed)
			return
		}

		next.ServeHTTP(response, request)
	})
}

func MiddlewareLogging(next http.Handler) http.Handler {
	return http.HandlerFunc(func(response http.ResponseWriter, request *http.Request) {
		next.ServeHTTP(response, request)

		zaplog.Throw(request.Method, request.URL.String(), http.StatusOK, request, response).
			Info("Выполнен запрос с IP адреса: " + request.RemoteAddr)
	})
}
