package resources

type CandidateResponse struct {
	// Код ответа на запрос
	Code int `json:"code"`
	// Данные
	Data interface{} `json:"data"`
	// Сообщение об ошибке
	Errors interface{} `json:"errors,omitempty"`
	// Ответ на запрос
	Status string `json:"status"`
}
