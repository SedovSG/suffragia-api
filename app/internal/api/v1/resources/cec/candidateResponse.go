package cec

type CandidateResponse struct {
	Embedded struct {
		CandidateDtoList []struct {
			Index    int    `json:"index"`
			Vrn      int64  `json:"vrn"`
			Fio      string `json:"fio"`
			Datroj   string `json:"datroj"`
			Vidvig   string `json:"vidvig"`
			Registr  string `json:"registr"`
			Vrnio    int64  `json:"vrnio,omitempty"`
			Namio    string `json:"namio"`
			Numokr   int    `json:"numokr"`
			Tekstat2 string `json:"tekstat2"`
			Links    struct {
				Self struct {
					Href string `json:"href"`
				} `json:"self"`
			} `json:"_links"`
		} `json:"candidateDtoList"`
	} `json:"_embedded"`
	Links struct {
		Self struct {
			Href string `json:"href"`
		} `json:"self"`
	} `json:"_links"`
	Page struct {
		Size          int `json:"size"`
		TotalElements int `json:"totalElements"`
		TotalPages    int `json:"totalPages"`
		Number        int `json:"number"`
	} `json:"page"`
}
