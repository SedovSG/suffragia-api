package cec

type ElectionResponse []struct {
	Vrn       string `json:"vrn"`
	Date      string `json:"date"`
	Name      string `json:"name"`
	SubjCode  string `json:"subjCode"`
	Pronetvd  string `json:"pronetvd"`
	Vidvibref string `json:"vidvibref"`
	VrnKomis  string `json:"vrnkomis"`
}
