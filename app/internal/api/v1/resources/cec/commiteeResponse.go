package cec

type CommitteResponse struct {
	Vrn      string `json:"vrn"`
	Name     string `json:"name"`
	SubjCode string `json:"subjCode"`
	NumKsa   string `json:"numKsa"`
	Vid      string `json:"vid"`
	Address  struct {
		Address string `json:"address"`
		Descr   string `json:"descr"`
		Phone   string `json:"phone"`
		Lat     string `json:"lat"`
		Lon     string `json:"lon"`
	} `json:"address"`
	VotingAddress struct {
		Address string `json:"address"`
		Descr   string `json:"descr"`
		Phone   string `json:"phone"`
		Lat     string `json:"lat"`
		Lon     string `json:"lon"`
	} `json:"votingAddress"`
}
