package cec

type ComissionResponse struct {
	Embedded struct {
		TvdDtoList []struct {
			Vrn      int64  `json:"vrn"`
			Namtvd   string `json:"namtvd,omitempty"`
			Namik    string `json:"namik"`
			Numtvd   string `json:"numtvd"`
			Vidtvd   string `json:"vidtvd"`
			Selected bool   `json:"selected"`
			Links    struct {
				Results struct {
					Href string `json:"href"`
				} `json:"results"`
				ResultsUik struct {
					Href string `json:"href"`
				} `json:"results.uik,omitempty"`
			} `json:"_links"`
		} `json:"tvdDtoList"`
	} `json:"_embedded"`
	Links struct {
		Self struct {
			Href string `json:"href"`
		} `json:"self"`
	} `json:"_links"`
}
