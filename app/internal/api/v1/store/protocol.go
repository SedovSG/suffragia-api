package store

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Protocol struct {
	// Идентификатор протокола
	ID primitive.ObjectID `bson:"id"`
	// Номер телефона наблюдателя
	Phone string `bson:"phone"`
	// Результаты референдума
	Data struct {
		// Рейтинги кандидатов
		CandidateRating []interface{} `bson:"candidate_rating"`
		// Список кандидатов и голосов
		CandidateList []interface{} `bson:"candidate_list"`
		// Дата проведения референдума
		Date string `bson:"date"`
		// Число бюллетеней, полученных УИК
		NumBallots string `bson:"num_ballots"`
		// Число бюллетеней в стационарных ящиках
		NumBallotsInBoxes string `bson:"num_ballots_in_boxes"`
		// Число бюллетеней в переносных ящиках
		NumBallotsInPortableBoxes string `bson:"num_ballots_in_portable_boxes"`
		// Число бюллетеней для голосования в помещении
		NumBallotsIndoor string `bson:"num_ballots_indoor"`
		// Число бюллетеней для голосования вне помещения
		NumBallotsOutdoor string `bson:"num_ballots_outdoor"`
		// Число уничтоженных бюллетеней
		NumDestoyBallots string `bson:"num_destoy_ballots"`
		// Число бюллетеней для голосования досрочно
		NumEarlyBallots string `bson:"num_early_ballots"`
		// Число недействительных бюллетеней
		NumInvalidBallots string `bson:"num_invalid_ballots"`
		// Число утраченных бюллетеней
		NumLostBallots string `bson:"num_lost_ballots"`
		// Число неучтённых бюллетеней
		NumUnaccountedBallots string `bson:"num_unaccounted_ballots"`
		// Число действительных бюллетеней
		NumValidBallots string `bson:"num_valid_ballots"`
		// Число избирателей на момент окончания голосования
		NumVoters string `bson:"num_voters"`
		// Идентификатор УИК
		UikID string `bson:"uik_id"`
		// Номер УИК
		UikNumber string `bson:"uik_number"`
	} `bson:"data"`
}
