package controllers

import (
	"net/http"

	"suffragia/internal/api/v1/services"
	"suffragia/pkg/utils"

	"github.com/SedovSG/zaplog"
)

//nolint:gochecknoglobals // Доступна в пределах текущего пакета
var protocol = new(services.Protocols)

// SaveProtocol сохраняе протокол голосования.
func SaveProtocol(resp http.ResponseWriter, req *http.Request) {
	res, err := protocol.Save(resp, req)
	if err != nil {
		zaplog.Throw(resp, req).Error(err.Error())
		return
	}

	utils.Response(resp).EncodeToJSON(res)
}

// GetProtocol получает результаты протоколов.
func GetProtocol(resp http.ResponseWriter, req *http.Request) {
	res, err := protocol.Get(resp, req)
	if err != nil {
		zaplog.Throw(resp, req).Error(err.Error())
		return
	}

	utils.Response(resp).EncodeToJSON(res)
}

// GetTotal получает кол-во голосов за кандидата.
func GetTotal(resp http.ResponseWriter, req *http.Request) {
	res, err := protocol.Total(resp, req)
	if err != nil {
		zaplog.Throw(resp, req).Error(err.Error())
		return
	}

	utils.Response(resp).EncodeToJSON(res)
}
