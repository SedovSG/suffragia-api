package controllers

import (
	"net/http"

	"suffragia/internal/api/v1/services"
	"suffragia/pkg/utils"

	"github.com/SedovSG/zaplog"
)

//nolint:gochecknoglobals // Доступна в пределах текущего пакета
var elections = new(services.Election)

// GetElections получает информацию об УИК.
func GetElections(resp http.ResponseWriter, req *http.Request) {
	res, err := elections.Get(resp, req)
	if err != nil {
		zaplog.Throw(resp, req).Error(err.Error())
		return
	}

	utils.Response(resp).EncodeToJSON(res)
}
