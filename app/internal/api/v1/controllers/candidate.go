package controllers

import (
	"net/http"

	"suffragia/internal/api/v1/services"
	"suffragia/pkg/utils"

	"github.com/SedovSG/zaplog"
)

//nolint:gochecknoglobals // Доступна в пределах текущего пакета
var candidates = new(services.Candidate)

// GetCandidates получает информацию о кандидатах.
func GetCandidates(resp http.ResponseWriter, req *http.Request) {
	res, err := candidates.Get(resp, req)
	if err != nil {
		zaplog.Throw(resp, req).Error(err.Error())
		return
	}

	utils.Response(resp).EncodeToJSON(res)
}
