package controllers

import (
	"net/http"

	"suffragia/internal/api/v1/services"
	"suffragia/pkg/utils"

	"github.com/SedovSG/zaplog"
)

//nolint:gochecknoglobals // Доступна в пределах текущего пакета
var comissions = new(services.Comission)

// GetComissions получает информацию об комиссиях.
func GetComissions(resp http.ResponseWriter, req *http.Request) {
	res, err := comissions.Get(resp, req)
	if err != nil {
		zaplog.Throw(resp, req).Error(err.Error())
		return
	}

	utils.Response(resp).EncodeToJSON(res)
}
