package controllers

import (
	"net/http"
)

// Метод для проверки доступности сервера.
func Availability(response http.ResponseWriter, request *http.Request) {
	response.WriteHeader(http.StatusOK)
}
