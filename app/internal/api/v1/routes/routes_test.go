package routes_test

import (
	"io"
	"net/http/httptest"
	"testing"

	"suffragia/internal/api/v1/routes"

	"github.com/stretchr/testify/assert"
)

type suites []struct {
	name   string
	method string
	uri    string
	query  string
	body   io.Reader
	code   int
}

func TestRouters(t *testing.T) {
	cases := suites{
		{"Availably", "HEAD", "/api/v1/", "", nil, 200},
	}

	for _, expected := range cases {
		t.Run(expected.name, func(t *testing.T) {
			req := httptest.NewRequest(expected.method, expected.uri+expected.query, expected.body)
			req.Header.Set("Accept", "application/json")
			req.Header.Set("X-Organisation", "0")
			req.Header.Set("X-Language", "ac")

			w := httptest.NewRecorder()
			routes.Init().ServeHTTP(w, req)

			assert.Equal(t, expected.code, w.Result().StatusCode)
		})
	}
}
