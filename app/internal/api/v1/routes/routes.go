package routes

import (
	"net/http"
	"os"

	"suffragia/internal/api/v1/controllers"
	"suffragia/internal/api/v1/middlewares"

	"github.com/SedovSG/zaplog"
	"github.com/gorilla/mux"
)

// Init обрабатывает маршруты приложения.
func Init() http.Handler {
	Mux := mux.NewRouter()

	prefix := "/api/v1"

	router := Mux.PathPrefix(prefix).Subrouter()

	router.HandleFunc("/{spec:swagger_[0-9]+\\.json}", func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		http.ServeFile(w, r, "api/openapi-spec/"+vars["spec"])
	})

	router.HandleFunc("/{spec:swagger_[0-9]+\\.yaml}", func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		http.ServeFile(w, r, "api/openapi-spec/"+vars["spec"])
	})

	router.NotFoundHandler = http.HandlerFunc(func(response http.ResponseWriter, request *http.Request) {
		response.WriteHeader(http.StatusBadRequest)

		if _, error := response.Write([]byte(`{"code": "400", "message": "Bad Request", "data": {}}`)); error != nil {
			zaplog.Throw().Error("Ошибка установки заголовков: " + error.Error())
		}
	})

	router.HandleFunc("/swagger.json", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "swagger.json")
	})

	router.HandleFunc("/swagger.yaml", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "swagger.yaml")
	})

	// swagger:operation HEAD / Available Availability
	//
	// Метод проверят доступность микросервиса
	//
	// Could be any pet
	//
	// ---
	// summary: Проверка доступности сервера.
	// description: "Запрос проверки доступности сервера."
	//
	// consumes:
	// - application/json
	//
	// produces:
	// - application/json
	//
	// responses:
	//   "200":
	//     description: "OK"
	//
	//   "400":
	//      description: "Bad Request"
	//
	//   "405":
	//      description: "Method Not Allowed"
	//
	//   "415":
	//     description: "Unsupported Media Type"
	router.HandleFunc("/", controllers.Availability).Methods("HEAD")

	// swagger:operation POST /protocols Protocols AddProtocol
	//
	// ---
	// summary: Добавление протокола голосования.
	// description: "Запрос на добавление протокола голосования"
	//
	// consumes:
	// - application/json
	//
	// produces:
	// - application/json
	//
	// parameters:
	// - in: body
	//   name: protocol
	//   description: Тело запроса
	//   required: true
	//   schema:
	//     "$ref": "#/definitions/Protocol"
	//
	// responses:
	//   "200":
	//     description: OK
	//     schema:
	//       "$ref": "#/definitions/ProtocolResponse"
	//
	//   "400":
	//      description: "Bad Request"
	//      schema:
	//        "$ref": "#/definitions/Response"
	//
	//   "405":
	//      description: "Method Not Allowed"
	//
	//   "415":
	//      description: "Unsupported Media Type"
	router.HandleFunc("/protocols", controllers.SaveProtocol).Methods("POST")

	// swagger:operation GET /protocols Protocols GetProtocol
	//
	// ---
	// summary: Получение протоколов голосования.
	// description: "Запрос на получение протоколов голосования"
	//
	// consumes:
	// - application/json
	//
	// produces:
	// - application/json
	//
	// parameters:
	// - name: filter
	//   in: query
	//   required: false
	//   type: string
	//   description: Фильтр элементов
	//   enum:
	//     - data.num_ballots::string
	//     - phone::string
	//     - data.date::2020-07-03
	//     - _id::int
	//
	// - name: sort
	//   in: query
	//   required: false
	//   type: string
	//   description: Сортировка значений
	//   enum:
	//     - _id::asc|desc
	//
	// - name: start
	//   in: query
	//   required: false
	//   type: string
	//   description: Начальное значение на странице
	//
	// - name: limit
	//   in: query
	//   required: false
	//   type: string
	//   description: Ограничение кол-ва значений на странице
	//
	// responses:
	//   "200":
	//     description: OK
	//     schema:
	//       "$ref": "#/definitions/PaginatorResponse"
	//
	//   "400":
	//      description: "Bad Request"
	//      schema:
	//        "$ref": "#/definitions/Response"
	//
	//   "405":
	//      description: "Method Not Allowed"
	//
	//   "415":
	//      description: "Unsupported Media Type"
	router.HandleFunc("/protocols", controllers.GetProtocol).Methods("GET")

	// swagger:operation GET /totals Protocols GetTotal
	//
	// ---
	// summary: Получение кол-во голосов за кандидата.
	// description: "Запрос на получение кол-ва голосов за кандидата"
	//
	// consumes:
	// - application/json
	//
	// produces:
	// - application/json
	//
	// responses:
	//   "200":
	//     description: OK
	//     schema:
	//       "$ref": "#/definitions/PaginatorResponse"
	//
	//   "400":
	//      description: "Bad Request"
	//      schema:
	//        "$ref": "#/definitions/Response"
	//
	//   "405":
	//      description: "Method Not Allowed"
	//
	//   "415":
	//      description: "Unsupported Media Type"
	router.HandleFunc("/totals", controllers.GetTotal).Methods("GET")

	// swagger:operation GET /cec/elections Cec GetElections
	//
	// ---
	// summary: Получение информации о предстоящих выборах.
	// description: "Запрос на получение информации о предстоящих выборах"
	//
	// consumes:
	// - application/json
	//
	// produces:
	// - application/json
	//
	// parameters:
	// - name: subject_id
	//   in: query
	//   required: false
	//   type: string
	//   description: КЛАДР код субъекта РФ
	//
	// - name: uik
	//   in: query
	//   required: false
	//   type: string
	//   description: Номер УИК
	//
	// responses:
	//   "200":
	//     description: OK
	//     schema:
	//       "$ref": "#/definitions/ElectionResponse"
	//
	//   "400":
	//      description: "Bad Request"
	//      schema:
	//        "$ref": "#/definitions/Response"
	//
	//   "405":
	//      description: "Method Not Allowed"
	//
	//   "415":
	//      description: "Unsupported Media Type"
	router.HandleFunc("/cec/elections", controllers.GetElections).Methods("GET").
		Queries("subject_id", "{subject_id:[0-9]+?}").
		Queries("uik", "{uik:[0-9]+?}")

	// swagger:operation GET /cec/candidates Cec GetCandidates
	//
	// ---
	// summary: Получение информации о кандидатах.
	// description: "Запрос на получение информации о кандидатах"
	//
	// consumes:
	// - application/json
	//
	// produces:
	// - application/json
	//
	// parameters:
	// - name: vrn_komis
	//   in: query
	//   required: true
	//   type: string
	//   description: VRN код комиссии
	//
	// - name: vrn_election
	//   in: query
	//   required: false
	//   type: string
	//   description: VRN код выборов
	//
	// responses:
	//   "200":
	//     description: OK
	//     schema:
	//       "$ref": "#/definitions/CandidateResponse"
	//
	//   "400":
	//      description: "Bad Request"
	//      schema:
	//        "$ref": "#/definitions/Response"
	//
	//   "405":
	//      description: "Method Not Allowed"
	//
	//   "415":
	//      description: "Unsupported Media Type"
	router.HandleFunc("/cec/candidates", controllers.GetCandidates).Methods("GET").
		Queries("vrn_election", "{vrn_election:[0-9]+?}").
		Queries("num_tvd", "{num_tvd:[0-9]+?}")

	// swagger:operation GET /cec/comissions Cec GetComissions
	//
	// ---
	// summary: Получение информации о комиссиях.
	// description: "Запрос на получение информации о комиссиях"
	//
	// consumes:
	// - application/json
	//
	// produces:
	// - application/json
	//
	// parameters:
	// - name: vrn_election
	//   in: query
	//   required: true
	//   type: string
	//   description: VRN код выборов
	//
	// - name: vrn_komis
	//   in: query
	//   required: true
	//   type: string
	//   description: VRN код комиссии
	//
	// responses:
	//   "200":
	//     description: OK
	//     schema:
	//       "$ref": "#/definitions/ComissionResponse"
	//
	//   "400":
	//      description: "Bad Request"
	//      schema:
	//        "$ref": "#/definitions/Response"
	//
	//   "405":
	//      description: "Method Not Allowed"
	//
	//   "415":
	//      description: "Unsupported Media Type"
	router.HandleFunc("/cec/comissions", controllers.GetComissions).Methods("GET").
		Queries("vrn_election", "{vrn_election:[0-9]+?}").
		Queries("vrn_komis", "{vrn_komis:[0-9]+?}")

	router.Use(middlewares.Middleware)
	router.Use(middlewares.MiddlewareLogging)

	host := os.Getenv("HOST")
	port := ":" + os.Getenv("PORT")

	zaplog.Throw(http.StatusOK, "HEAD", "http://"+host+port+prefix+"/").Info("Сервер запущен")

	return router
}
