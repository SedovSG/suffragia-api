// Package classification Suffragia API.
//
// RESTful API микросервис используется для хранения голосов референдума,
// в виде протоколов, их обработки и анализа для фронтенд части.
//
// Ниже представлены возможные команды и запросы к сервису.
//
// Terms Of Service:
//
//     Schemes: http
//     Host: localhost
//     BasePath: /api/v1/
//     Version: 1.0.0
//     Contact: Sedov Stanislav<sedov@arnica.pro>
//     License: MIT http://opensource.org/licenses/MIT
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
// swagger:meta
package main

import (
	"net/http"
	"os"

	"suffragia/internal/api/v1/routes"

	"github.com/SedovSG/zaplog"
	"github.com/asaskevich/govalidator"
)

func init() {
	govalidator.SetFieldsRequiredByDefault(true)
}

func main() {
	http.Handle("/", routes.Init())

	port := ":" + os.Getenv("PORT")

	if errs := http.ListenAndServe(port, nil); errs != nil {
		zaplog.Throw().Fatal("Ошибка запуска сервера: " + errs.Error())
	}
}
