#!/bin/bash
set -eu

if [[ "$ENV" == "DEV" ]]; then
	make setup mode=dev

  	CompileDaemon -build="go build -o /go/src/bin/main ." -log-prefix=false -exclude-dir=".git" -graceful-kill=true -command="/go/src/bin/main"
elif [[ "$ENV" == "CI" ]]; then
	make setup mode=ci && go build -o /go/src/bin/main . && /go/src/bin/main
fi
