# Микросервис для учёта голосов референдума

Данный микросервис используется для хранения голосов референдума, в виде протоколов,
их обработки и анализа для фронтенд части.

## Структура

* Docker: версия 19.03
* Docker Compose: версия 1.25
* Базовый образ: [debian:bullseye](https://www.debian.org/releases/stable/)
* Зависимости: Go 1.17.8, MongoDB 4.2.12, GoSwagger 0.29
* Go-пакеты: [gorilla/mux](https://github.com/gorilla/mux), [githubnemo/CompileDaemon](github.com/githubnemo/CompileDaemon), [asaskevich/govalidator](https://github.com/asaskevich/govalidator), [stretchr/testify](github.com/stretchr/testify), [rakyll/gotest](github.com/rakyll/gotest), [golangci-lint](github.com/golangci/golangci-lint/cmd/golangci-lint), [mongo-driver](go.mongodb.org/mongo-driver/mongo), [SedovSG/zaplog](github.com/SedovSG/zaplog)

### Директории

<pre>
/api                    Спецификации OpenAPI/Swagger, файлы JSON schema, файлы определения протоколов.
/internal               Внутренний код приложения и библиотек.
.../api/v1/controllers  Контроллеры.
.../api/v1/routers      Маршруты.
.../api/v1/middlewares  Посредники, фильтры обработки HTTP-запросов.
.../api/v1/models       Структуры моделей для взаимодействия с БД.
.../api/v1/services     Бизнес логика.
.../api/v1/resources    Структуры запросов.
/pkg                    Код библиотек, пригодных для использования в сторонних приложениях.
main.go
</pre>

## Использование

Для начала ставим себе сам Docker, подробности тут: [https://docs.docker.com/install/linux/docker-ce/ubuntu/](https://docs.docker.com/install/linux/docker-ce/ubuntu/)

Также необходимо установить Docker Compose: [https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/)

Кроме этого необходом установить Git глобально: [https://git-scm.com/book/en/v2/Getting-Started-Installing-Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

Проект поддерживает следующие среды: **разработки**, **интеграции**, **производства**.

### I Развёртывание для разработки

#### 1. Для начала необходимо получить проект:
```bash
$: git clone git@gitlab.com:SedovSG/suffragia-api.git suffragia
```

#### 2. Создать файл настроек среды окружения:
```bash
$: cd suffragia && cp .env.dist .env
```

#### 3. Запускаем контейнер:
```bash
$: make run
```

Либо в режиме демона:
```bash
$: make build
```

При этом создадутся следующие контейнеры:

* **suffragia_api-dev**
* **suffragia_mongo**

#### 4. Запускаем тесты
```bash
$: make test
```

#### 5. URL для запуска проекта: [localhost:9111](http://localhost:9111)

#### 6. Остановить контейнер:
```bash
$: make down
```

Разработка ведётся в директории `app`.

В режиме разработки используется [демон компиляции](https://github.com/githubnemo/CompileDaemon), автоматизирующий сборку при каждом обновлении файлов проекта.

### II Развёртывание для интеграции

Основное различие заключается в наличии `docker-compose.yml`, как базового, для наследования других окружений, так и файлов `docker-compose.prod.yml` для производства, `docker-compose.stage.yml` для интеграции и отдельно `docker-compose.override.yml` для разработки.

Предрелиз — это такая среда, в которой происходит проверка перед выпуском продукта. Её особенностью является то, что она максимально приближена к условиям производственной среды, что дает возможность полнее протестировать то, что происходит. Обычно это то место, куда идут менеджеры, тестировщики, заказчики. Часто интеграция выполняет сразу две задачи. Как проверку конкретных фич от разработчиков, так и окончательный прогон приложения перед релизом.

**Поэтому работа с проектом для других сред немного отличается**:

#### 1. Сборка проекта для интеграции:
```bash
$: make run mode=stage
```

#### 2. Запуск контейнера интеграции:
```bash
$: make build mode=stage
```

#### 3. URL для запуска проекта: [localhost:9111](http://localhost:9111)

#### 4. Остановка контейнера интеграции:
```bash
$: make down
```

### III Развёртывание для производства

#### 1. Сборка проекта для производства:
```bash
$: make run mode=prod
```

#### 2. Запуск контейнера производства:
```bash
$: make build mode=prod
```

#### 3. URL для запуска проекта: [localhost:9111](http://localhost:9111)

#### 4. Остановка контейнера производства:
```bash
$: make down
```

### Утилита make

Для того чтобы не запоминать все консольные команды необходимые для работы и структурирования процесса разработки используется команды из файла **Makefile**

1. Просмотр всех доступных команд:
```bash
$: make
```
