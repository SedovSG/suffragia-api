envfile ?= .env
type ?= yaml
user ?= ${UNAME}
name ?= api

ifneq ("$(wildcard $(envfile))","")
include $(envfile)
export $(shell sed 's/=.*//' $(envfile))
endif

DATE=`date +"%Y%m%d"`
MONGO_DUMP_DIRNAME="/home/docker/mongo/dump"
MONGO_DUMP="${MONGO_DUMP_DIRNAME}/${COMPOSE_PROJECT_NAME}_${DATE}.dump"
timestamp=$(shell date +%s)

.PHONY: run
run: ## Собрать все контейнеры приложения: mode=stage|prod
ifeq ($(mode),prod)
	@docker compose -f docker-compose.yml -f docker-compose.prod.yml up
else ifeq ($(mode),stage)
	@docker compose -f docker-compose.yml -f docker-compose.stage.yml up
else
	@docker compose up
endif

.PHONY: build
build: ## Собрать все контейнеры приложения в фоновом режиме: mode=stage|prod
ifeq ($(mode),prod)
	@docker compose -f docker-compose.yml -f docker-compose.prod.yml up --build -d
else ifeq ($(mode),stage)
	@docker compose -f docker-compose.yml -f docker-compose.stage.yml up --build -d
else ifeq ($(mode),ci)
	@docker compose -f docker-compose.yml -f docker-compose.ci.yml up --build -d
else
	@docker compose up --build -d
endif

.PHONY: start
start: ## Запустить все конейнеры приложения
	@docker compose -f docker-compose.yml start

.PHONY: stop
stop: ## Остановить все конейнеры приложения
	@docker compose -f docker-compose.yml stop

.PHONY: restart
restart: ## Перезапустить все конейнеры приложения
	@docker compose -f docker-compose.yml restart

.PHONY: down
down: ## Уничтожить все конейнеры приложения
	@docker compose -f docker-compose.yml down

.PHONY: status
status: ## Просмотреть статусы контейнеров приложения
	@docker compose -f docker-compose.yml ps

.PHONY: watch
watch: ## Наблюдать за конейнером приложения: name=service
	@docker attach $$(docker compose ps | grep _$(name) | awk '{print $$1}')

.PHONY: logs
logs: ## Просмотреть лог контейнера приложения:  name=service
	@docker logs $$(docker compose ps | grep _$(name) | awk '{print $$1}')

.PHONY: bash
bash: ## Подключиться к конейнеру приложения: name=service [user=root]
ifeq ($(user),root)
	@docker exec -itu root $$(docker compose ps | grep _$(name) | awk '{print $$1}') bash
else
	@docker exec -it $$(docker compose ps | grep _$(name) | awk '{print $$1}') bash
endif

.PHONY: mongo
mongo: ## Подключиться к контейнеру БД MongoDB: user=root
	@docker exec -it "${COMPOSE_PROJECT_NAME}_mongo" bash -c 'mongo -u ${MONGO_ROOT_USERNAME} \
		-p ${MONGO_ROOT_PASSWORD} --host localhost --authenticationDatabase admin --port 27017 ${MONGO_DATABASE}'

.PHONY: swagger
swagger: ## Открыть интерактивную докментацию Swagger: name=ui|editor [type=yaml|json]
	@if [ ! -f "${PWD}/app/api/openapi-spec/swagger_${timestamp}.$(type)" ]; then \
		docker exec -i "${COMPOSE_PROJECT_NAME}_api-dev" bash -c 'make swagger type=$(type) prefix=${timestamp}'; \
	fi

ifeq ($(name),editor)
	@xdg-open "http://91.201.54.61:4330?url=http://${HOST}:${PORT}/api/v1/swagger_${timestamp}.$(type)"
else
	@xdg-open "http://91.201.54.61:4331?url=http://${HOST}:${PORT}/api/v1/swagger_${timestamp}.$(type)"
endif

.PHONY: dump-import
dump-import: ## Создать дамп БД
	@mkdir -p ${MONGO_DUMP_DIRNAME}
	@docker exec -i "${COMPOSE_PROJECT_NAME}_mongo" bash -c 'mongodump --archive \
		-u ${MONGO_ROOT_USERNAME} -p ${MONGO_ROOT_PASSWORD} --host localhost --authenticationDatabase admin \
		--port 27017' > ${MONGO_DUMP}
	@echo "Дамп БД успешно создан: ${MONGO_DUMP}"

.PHONY: dump-export
dump-export: ### Разворачивание дампа БД: dump-export file=name
	@docker exec -i "${COMPOSE_PROJECT_NAME}_mongo" bash -c 'mongodump --archive \
		-u ${MONGO_ROOT_USERNAME} -p ${MONGO_ROOT_PASSWORD} --host localhost --authenticationDatabase admin \
		--port 27017' < "${MONGO_DUMP_DIRNAME}/$(file)"
	@echo "Дамп БД успешно развёрнут"

.PHONY: format
format: ## Отформатировать исходный код
	@docker exec -it $$(docker compose ps | grep '_api' | awk '{print $$1}') bash -c 'make format'

.PHONY: lint
lint: ## Проверить исходный код на соответствие стандартам
	@docker exec -it $$(docker compose ps | grep '_api' | awk '{print $$1}') bash -c "make lint"

.PHONY: test
test: ## Запустить выполнение тестов
	@docker exec -i $$(docker compose ps | grep '_api' | awk '{print $$1}') bash -c 'make test'

.PHONY: cover
cover: ## Запустить отчёт о покрытии кода тестами
	@docker exec -it $$(docker compose ps | grep '_api' | awk '{print $$1}') bash -c 'make cover type=$(type)'

ifeq ($(type),html)
	@xdg-open ./app/coverage.html
endif

.PHONY: wait
wait: ## Ожидать завершение сборки
	@while ! make logs 2>&1 | grep -i 'Зависимости успешно установлены' > /dev/null; do sleep 2; done;
	@echo "Загрузка зависимостей завершена"

.PHONY: help
help: ## Показать справку
	@ echo '  Использование: make [КЛЮЧ...] [ОПЦИЯ...]'
	@ echo ''
	@ echo '  Ключи:'
	@ grep -E '^[a-zA-Z_-]+:.*?## .*?$$' $(firstword $(MAKEFILE_LIST)) | awk 'BEGIN {FS = ":.*?## "}; \
	{printf "    \033[36m%-30s\033[0m %s\n", $$1, $$2, $$3}' | column -t -s ':'

.DEFAULT_GOAL := help
